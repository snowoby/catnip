package utils

import "math/rand"

// https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-go
func RandomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = Chars[rand.Intn(len(Chars))]
	}
	return string(b)
}


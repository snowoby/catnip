package utils

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"golang.org/x/crypto/pbkdf2"
	"strings"
)

func Auth(password string,record string) bool {
	split := strings.Split(record,"$")
	if len(split)!=4 {
		return false
	}
	return Encrypt(password,split[2])==record
}

func Encrypt(password, salt string) string {
	//	from django                              from django from django from django
	encrypted:=pbkdf2.Key([]byte(password),[]byte(salt),150000,32 ,sha256.New)
	e:=base64.StdEncoding.EncodeToString(encrypted)
	return fmt.Sprintf("%s$%d$%s$%s","pbkdf2_sha256", 150000, salt, e)
}

func PrintPretty(b interface{})  {
	a,err:=json.Marshal(b)
	if err!=nil {
		fmt.Print(err)
	}else {
		fmt.Printf(string(a))
	}

}

func CookieSplit(cookie string) (uint,string,error) {
	cookie=strings.Trim(cookie," \r\n")
	//result:=strings.Split(cookie,".")
}
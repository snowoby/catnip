package utils

import (
	log "github.com/sirupsen/logrus"
	"regexp"
	"strings"
)

const ReEmail = `^[a-zA-Z0-9.!#$%&'*+/=?^_\x60{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$`
const Minuscule = `abcdefghijklmnopqrstuvwxyz`
const Majuscule = `ABCDEFGHIJKLMNOPQRSTUVWXYZ`
const Number = `0123456789`
const SpecialCharacters = `~!@#$%^&*()_\x60-={}[]\|:"'';,./ `
const Chars = Minuscule + Majuscule + Number

func EmailValidator(email string) bool {
	log.Debug("email verify: ", email)
	if !CharactersValid(email) {
		return false
	}
	var re = regexp.MustCompile(ReEmail)
	return len(re.FindStringIndex(email))>0
}

func LoginNameValidator(loginName string) bool {
	return SimpleCharsVerify(loginName,64,1)
}

func SimpleCharsVerify(content string, max,min int) bool {
	return len(content)<=max&&len(content)>=min&& CharactersValid(content)
}

func PasswordValidator(password string) bool {
	if !CharactersValid(password) {
		return false
	}
	//Powered by Immigration Policy
	var score = -5
	if len(password)<=6 {
		score-=10
	}
	if len(password)>=1024 {
		score-=10
	}

	if len(password)>=10 {
		score+=2
	}
	if strings.ContainsAny(password, SpecialCharacters) {
		score+=2
	}
	if strings.ContainsAny(password, Minuscule) {
		score+=2
	}
	if strings.ContainsAny(password, Majuscule) {
		score+=2
	}
	if strings.ContainsAny(password, Number) {
		score+=2
	}
	log.Debug("password score: ", score)
	return score>0

}

func CharactersValid(content string) bool {
	return len(strings.ToValidUTF8(content,""))==len(content)
}

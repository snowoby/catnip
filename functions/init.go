package functions

import (
	"catnip/structs"
	"context"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"log"
)
const Images = "images"
var client *minio.Client

func init()  {
	storage:=structs.Config.ObjectStorage
	var err error
	client, err = minio.New(storage.Endpoint, &minio.Options{
		Creds:        credentials.NewStaticV4(storage.AccessKey, storage.SecretKey, ""),
		Secure:       storage.SSL,
		Region:       storage.Location,
	})

	if err!=nil {
		panic("storage error "+err.Error())
	}

	for _,i := range []string{Images} {
		createBucket(i)
	}

}

func createBucket(name string) {
	ctx:=context.Background()
	exists, err := client.BucketExists(ctx, name)
	if err == nil && exists {
		log.Print("bucket "+name+" exists")
	}else if err!=nil {
		panic("bucket "+name+" create failed (exists): "+ err.Error())
	}else{
		err=client.MakeBucket(ctx, name , minio.MakeBucketOptions{
			Region:        structs.Config.ObjectStorage.Location,
		})
		if err!=nil {
			panic("bucket "+name+" create failed: "+ err.Error())
		}else {
			log.Print("bucket "+name+" created.")
		}
	}


}
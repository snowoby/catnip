package functions

import (
	"bytes"
	"catnip/structs"
	"context"
	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
)

func upload(ctx context.Context, data *[]byte, file *structs.File) error {
	reader := bytes.NewReader(*data)
	uploadInfo,err:=client.PutObject(ctx, file.Bucket, file.Filename(), reader, int64(file.Size), minio.PutObjectOptions{
		ContentType: file.MIME,
	})
	log.Debug(uploadInfo)
	return err
}
package functions

import (
	"catnip/db"
	"catnip/ent"
	"catnip/structs"
	"catnip/utils"
	"errors"
	"github.com/gin-gonic/gin"
	"strconv"
	"strings"
)

func Register(ctx *gin.Context, account *structs.Account) (int,error) {
	account.Password=utils.Encrypt(account.Password,utils.RandomString(16))
	return db.NewAccount(ctx,account)
}

func Login(ctx *gin.Context, account *ent.Account) (*structs.Cookie,error) {
	accountRecord, err:= db.FindAccountByUsername(ctx,account.Username)
	if err!=nil {
		return nil, err
	}

	if !utils.Auth(account.Password, accountRecord.Password) {
		return nil,errors.New("auth failed")
	}

	return db.CreateCookie(ctx, accountRecord.ID)
}

func VerifyCookie(ctx *gin.Context, c string) (*structs.Account,error) {
	seg:=strings.Split(".",c)
	if len(seg)!=2 {
		return nil, errors.New("cookie format error")
	}
	id,err:=strconv.Atoi(seg[0])
	if err!=nil {
		return nil, err
	}
	return db.VerifyCookie(ctx,c,id)
}
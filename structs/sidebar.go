package structs

type Sidebar struct {
	ID       uint `gorm:"primary_key;AUTO_INCREMENT"`
	Text string
	Link string
	ImagePath string
	Width string
	Order string
}

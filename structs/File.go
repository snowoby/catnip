package structs

import (
	"fmt"
	"time"
)

type File struct {
	OriginalFilename string `json:"original_filename"`
	UploadedAt time.Time `json:"uploaded_at"`
	Bucket string `json:"bucket"`
	UUID string `json:"uuid"`
	Annotation string `json:"annotation"`
	Size int `json:"size"`
	MIME string `json:"mime"`
	ControlID string `json:"control_id"`
	Extension string `json:"extension"`
}

func (f *File) Filename() string {
	return fmt.Sprintf("%s.%s.%d.%s",f.UUID,f.Annotation,f.Size,f.Extension)
}
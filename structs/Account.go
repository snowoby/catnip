package structs

import (
	"fmt"
	"time"
)

type Account struct {
	ID int `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Email string `json:"email"`
}

type AccountSafe struct {
	ID int `json:"id"`
	Username string `json:"username"`
}

type Cookie struct {
	ID int `json:"id"`
	CookieString string `json:"cookie"`
	Expire time.Time `json:"expire"`
	ClientName string `json:"client_name"`
}

func (c *Cookie) ToString() string {
	return fmt.Sprintf("%d.%s",c.ID,c.CookieString)
}
package structs

type Profile struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Front string `json:"front"`
	Behind string `json:"behind"`
	Photo string `json:"photo"`
	CallSign string `json:"call_sign"`
	Callable bool `json:"callable"`
}

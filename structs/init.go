package structs

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

var Config struct{
	ObjectStorage struct {
		AccessKey string `json:"access_key"`
		SecretKey string `json:"secret_key"`
		Endpoint string  `json:"endpoint"`
		SSL bool `json:"ssl"`
		Location string `json:"location"`
	} `json:"object_storage"`
}

func init() {
	config, err := os.Open("config.json")
	if err!=nil {
		panic("config error"+err.Error())
	}
	value, _:=ioutil.ReadAll(config)
	err=json.Unmarshal(value, &Config)
	if err!=nil {
		panic("config format error"+err.Error())
	}
}

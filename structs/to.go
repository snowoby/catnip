package structs

func (a *Account) ToSafe () *AccountSafe {
	return &AccountSafe{
		ID:       a.ID,
		Username: a.Username,
	}
}
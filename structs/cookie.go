package structs

import "time"

type Cookie struct {
	ID       uint `gorm:"primary_key;AUTO_INCREMENT"`
	UserID uint
	Cookie string
	Expire time.Time
	LastLogin time.Time
	Device string
	IP string
}
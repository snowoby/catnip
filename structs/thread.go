package structs

import "time"

type Thread struct {
	ID       uint `gorm:"primary_key;AUTO_INCREMENT"`
	OwnerID uint
	Owner *Profile `gorm:"foreignkey:OwnerID"`
	Title string
	CoverImagePath string
	PublishTime time.Time
	BelongsTo uint
	ReplyTo uint
	Anonymous bool
	Path string
	Draft bool
	ThreadUnits []*ThreadUnit
}

type ThreadUnit struct {
	ID       uint `gorm:"primary_key;AUTO_INCREMENT"`
	ThreadID uint
	Content string
	PublishTime time.Time
	Order int
}

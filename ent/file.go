// Code generated by entc, DO NOT EDIT.

package ent

import (
	"catnip/ent/file"
	"catnip/ent/profile"
	"fmt"
	"strings"
	"time"

	"github.com/facebook/ent/dialect/sql"
)

// File is the model entity for the File schema.
type File struct {
	config `json:"-"`
	// ID of the ent.
	ID int `json:"id,omitempty"`
	// OriginalFilename holds the value of the "original_filename" field.
	OriginalFilename string `json:"original_filename,omitempty"`
	// UploadedAt holds the value of the "uploaded_at" field.
	UploadedAt time.Time `json:"uploaded_at,omitempty"`
	// Bucket holds the value of the "bucket" field.
	Bucket string `json:"bucket,omitempty"`
	// UUID holds the value of the "uuid" field.
	UUID string `json:"uuid,omitempty"`
	// Annotation holds the value of the "annotation" field.
	Annotation string `json:"annotation,omitempty"`
	// ControlID holds the value of the "control_id" field.
	ControlID string `json:"control_id,omitempty"`
	// Size holds the value of the "size" field.
	Size int `json:"size,omitempty"`
	// Mime holds the value of the "mime" field.
	Mime string `json:"mime,omitempty"`
	// Extension holds the value of the "extension" field.
	Extension string `json:"extension,omitempty"`
	// Edges holds the relations/edges for other nodes in the graph.
	// The values are being populated by the FileQuery when eager-loading is set.
	Edges         FileEdges `json:"edges"`
	profile_files *int
}

// FileEdges holds the relations/edges for other nodes in the graph.
type FileEdges struct {
	// Profile holds the value of the profile edge.
	Profile *Profile
	// loadedTypes holds the information for reporting if a
	// type was loaded (or requested) in eager-loading or not.
	loadedTypes [1]bool
}

// ProfileOrErr returns the Profile value or an error if the edge
// was not loaded in eager-loading, or loaded but was not found.
func (e FileEdges) ProfileOrErr() (*Profile, error) {
	if e.loadedTypes[0] {
		if e.Profile == nil {
			// The edge profile was loaded in eager-loading,
			// but was not found.
			return nil, &NotFoundError{label: profile.Label}
		}
		return e.Profile, nil
	}
	return nil, &NotLoadedError{edge: "profile"}
}

// scanValues returns the types for scanning values from sql.Rows.
func (*File) scanValues() []interface{} {
	return []interface{}{
		&sql.NullInt64{},  // id
		&sql.NullString{}, // original_filename
		&sql.NullTime{},   // uploaded_at
		&sql.NullString{}, // bucket
		&sql.NullString{}, // uuid
		&sql.NullString{}, // annotation
		&sql.NullString{}, // control_id
		&sql.NullInt64{},  // size
		&sql.NullString{}, // mime
		&sql.NullString{}, // extension
	}
}

// fkValues returns the types for scanning foreign-keys values from sql.Rows.
func (*File) fkValues() []interface{} {
	return []interface{}{
		&sql.NullInt64{}, // profile_files
	}
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the File fields.
func (f *File) assignValues(values ...interface{}) error {
	if m, n := len(values), len(file.Columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	value, ok := values[0].(*sql.NullInt64)
	if !ok {
		return fmt.Errorf("unexpected type %T for field id", value)
	}
	f.ID = int(value.Int64)
	values = values[1:]
	if value, ok := values[0].(*sql.NullString); !ok {
		return fmt.Errorf("unexpected type %T for field original_filename", values[0])
	} else if value.Valid {
		f.OriginalFilename = value.String
	}
	if value, ok := values[1].(*sql.NullTime); !ok {
		return fmt.Errorf("unexpected type %T for field uploaded_at", values[1])
	} else if value.Valid {
		f.UploadedAt = value.Time
	}
	if value, ok := values[2].(*sql.NullString); !ok {
		return fmt.Errorf("unexpected type %T for field bucket", values[2])
	} else if value.Valid {
		f.Bucket = value.String
	}
	if value, ok := values[3].(*sql.NullString); !ok {
		return fmt.Errorf("unexpected type %T for field uuid", values[3])
	} else if value.Valid {
		f.UUID = value.String
	}
	if value, ok := values[4].(*sql.NullString); !ok {
		return fmt.Errorf("unexpected type %T for field annotation", values[4])
	} else if value.Valid {
		f.Annotation = value.String
	}
	if value, ok := values[5].(*sql.NullString); !ok {
		return fmt.Errorf("unexpected type %T for field control_id", values[5])
	} else if value.Valid {
		f.ControlID = value.String
	}
	if value, ok := values[6].(*sql.NullInt64); !ok {
		return fmt.Errorf("unexpected type %T for field size", values[6])
	} else if value.Valid {
		f.Size = int(value.Int64)
	}
	if value, ok := values[7].(*sql.NullString); !ok {
		return fmt.Errorf("unexpected type %T for field mime", values[7])
	} else if value.Valid {
		f.Mime = value.String
	}
	if value, ok := values[8].(*sql.NullString); !ok {
		return fmt.Errorf("unexpected type %T for field extension", values[8])
	} else if value.Valid {
		f.Extension = value.String
	}
	values = values[9:]
	if len(values) == len(file.ForeignKeys) {
		if value, ok := values[0].(*sql.NullInt64); !ok {
			return fmt.Errorf("unexpected type %T for edge-field profile_files", value)
		} else if value.Valid {
			f.profile_files = new(int)
			*f.profile_files = int(value.Int64)
		}
	}
	return nil
}

// QueryProfile queries the profile edge of the File.
func (f *File) QueryProfile() *ProfileQuery {
	return (&FileClient{config: f.config}).QueryProfile(f)
}

// Update returns a builder for updating this File.
// Note that, you need to call File.Unwrap() before calling this method, if this File
// was returned from a transaction, and the transaction was committed or rolled back.
func (f *File) Update() *FileUpdateOne {
	return (&FileClient{config: f.config}).UpdateOne(f)
}

// Unwrap unwraps the entity that was returned from a transaction after it was closed,
// so that all next queries will be executed through the driver which created the transaction.
func (f *File) Unwrap() *File {
	tx, ok := f.config.driver.(*txDriver)
	if !ok {
		panic("ent: File is not a transactional entity")
	}
	f.config.driver = tx.drv
	return f
}

// String implements the fmt.Stringer.
func (f *File) String() string {
	var builder strings.Builder
	builder.WriteString("File(")
	builder.WriteString(fmt.Sprintf("id=%v", f.ID))
	builder.WriteString(", original_filename=")
	builder.WriteString(f.OriginalFilename)
	builder.WriteString(", uploaded_at=")
	builder.WriteString(f.UploadedAt.Format(time.ANSIC))
	builder.WriteString(", bucket=")
	builder.WriteString(f.Bucket)
	builder.WriteString(", uuid=")
	builder.WriteString(f.UUID)
	builder.WriteString(", annotation=")
	builder.WriteString(f.Annotation)
	builder.WriteString(", control_id=")
	builder.WriteString(f.ControlID)
	builder.WriteString(", size=")
	builder.WriteString(fmt.Sprintf("%v", f.Size))
	builder.WriteString(", mime=")
	builder.WriteString(f.Mime)
	builder.WriteString(", extension=")
	builder.WriteString(f.Extension)
	builder.WriteByte(')')
	return builder.String()
}

// Files is a parsable slice of File.
type Files []*File

func (f Files) config(cfg config) {
	for _i := range f {
		f[_i].config = cfg
	}
}

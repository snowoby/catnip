package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"time"
)

// Cookies holds the schema definition for the Cookies entity.
type Cookie struct {
	ent.Schema
}

// Fields of the Cookies.
func (Cookie) Fields() []ent.Field {
	return []ent.Field{
		field.String("cookie").NotEmpty(),
		field.Time("expire").Default(func() time.Time {
			return time.Now().Add(time.Hour*24*365)
		}),
		field.String("client_name").NotEmpty(),
	}
}

// Edges of the Cookies.
func (Cookie) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("account", Account.Type).
			Ref("cookies").Unique(),
	}
}


package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
)

// Profile holds the schema definition for the Profile entity.
type Profile struct {
	ent.Schema
}

// Fields of the Profile.
func (Profile) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").NotEmpty().MaxLen(255),
		field.String("front").Optional().NotEmpty().MaxLen(255),
		field.String("behind").Optional().NotEmpty().MaxLen(255),
		field.String("photo").Optional(),
		field.String("call_sign").MaxLen(16).Optional().Nillable().Unique(),
		field.Bool("callable").Default(true),
	}
}

// Edges of the Profile.
func (Profile) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("owner", Account.Type).Ref("profiles").Unique(),
		edge.To("files", File.Type),

	}
}

package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
)

type Account struct {
	ent.Schema
}

func (Account) Fields() []ent.Field {
	return []ent.Field{
		field.String("username").Unique().NotEmpty(),
		field.String("password").NotEmpty(),
		field.String("email").Unique(),
	}
}

func (Account) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("cookies", Cookie.Type),
		edge.To("profiles", Profile.Type),
	}
}

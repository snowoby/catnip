package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/edge"
	"github.com/facebook/ent/schema/field"
	"time"
)

// File holds the schema definition for the File entity.
type File struct {
	ent.Schema
}

// Fields of the File.
func (File) Fields() []ent.Field {
	return []ent.Field{
		field.String("original_filename").Default("noname"),
		field.Time("uploaded_at").Default(time.Now),
		field.String("bucket").NotEmpty(),
		field.String("uuid").NotEmpty(),
		field.String("annotation"),
		field.String("control_id"),
		field.Int("size"),
		field.String("mime"),
		field.String("extension"),

	}
}

// Edges of the File.
func (File) Edges() []ent.Edge {
	return  []ent.Edge{edge.From("profile", Profile.Type).
		Ref("files").Unique(),
	}
}

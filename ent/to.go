package ent

import (
	"catnip/structs"
)

func (a *Account) To () *structs.Account {
	return &structs.Account{
		ID:       a.ID,
		Username: a.Username,
		Password: a.Password,
		Email:    a.Email,
	}
}

func (c *Cookie) To () *structs.Cookie {
	return &structs.Cookie{
		ID:           c.ID,
		CookieString: c.Cookie,
		Expire:       c.Expire,
		ClientName:   c.ClientName,
	}
}

func (p *Profile) To () *structs.Profile {
	ter := &structs.Profile{
		ID:       p.ID,
		Name:     p.Name,
		Front:    p.Front,
		Behind:   p.Behind,
		Photo:    p.Photo,
		CallSign: "",
		Callable: p.Callable,
	}
	if p.CallSign!=nil {
		ter.CallSign=*p.CallSign
	}
	return ter
}

func (f *File) To () *structs.File {
	return &structs.File{
		OriginalFilename: f.OriginalFilename,
		UploadedAt:       f.UploadedAt,
		Bucket:           f.Bucket,
		UUID:             f.UUID,
		Annotation:       f.Annotation,
		Size:             f.Size,
		ControlID:		  f.ControlID,
		MIME:             f.Mime,
		Extension:        f.Extension,
	}
}
module catnip

go 1.14

require (
	github.com/appleboy/gin-jwt/v2 v2.6.3
	github.com/facebook/ent v0.4.2
	github.com/gin-gonic/gin v1.4.0
	github.com/h2non/bimg v1.1.4 // indirect
	github.com/jinzhu/gorm v1.9.14
	github.com/mattn/go-sqlite3 v1.14.2
	github.com/minio/minio-go/v7 v7.0.5 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.1
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
)

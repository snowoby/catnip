package db

import (
	"catnip/ent"
	"context"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

var client *ent.Client

func open() (*ent.Client, error) {
	client, err := ent.Open("sqlite3", "file:db.sqlite3?_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	// Run the auto migration tool.
	c := context.Background()
	if err := client.Schema.Create(c); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	return client,nil
}

func init()  {
	var err error
	client,err=open()
	if err!=nil {
		log.Fatal(err)
	}
}


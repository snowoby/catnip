package db

import (
	"catnip/ent/account"
	"catnip/ent/cookie"
	"catnip/structs"
	"catnip/utils"
	"errors"
	"github.com/gin-gonic/gin"
)

func NewAccount(ctx *gin.Context, account *structs.Account) (int,error) {
	tmp:= client.Account.
		Create().
		SetUsername(account.Username).
		SetPassword(account.Password)
	if account.Email!="" {
		tmp.SetEmail(account.Email)
	}
	acc,err:=tmp.Save(ctx)
	if err!=nil {
		return 0, err
	}
	return acc.ID,err
}

func FindAccountByUsername(ctx *gin.Context, username string) (*structs.Account,error) {
	acc,err:=client.Account.
		Query().
		Where(
			account.Username(username),
		).Only(ctx)
	if err!=nil {
		return nil, err
	}else {
		return acc.To(),nil
	}
}

func FindAccountByEmail(ctx *gin.Context, email string) (*structs.Account,error) {
	acc, err:=client.Account.
		Query().
		Where(
			account.Email(email),
		).Only(ctx)
	if err!=nil {
		return nil, err
	}else {
		return acc.To(),nil
	}
}

func CreateCookie(ctx *gin.Context, accountID int) (*structs.Cookie,error) {
	c,err:=client.Cookie.Create().
		SetCookie(utils.RandomString(128)).
		SetClientName(ctx.ClientIP()).
		SetAccountID(accountID).
		Save(ctx)
	if err!=nil {
		return nil, err
	}else {
		return c.To(),nil
	}
}

func VerifyCookie(ctx *gin.Context, c string, id int) (*structs.Account,error) {
	acc,err:=client.Cookie.
		Query().
		Where(cookie.ID(id),cookie.Cookie(c)).
		QueryAccount().
		Only(ctx)

	if err!=nil {
		return nil, err
	}else if acc.ID==0 {
		return nil, errors.New("cookie not found")
	}else {
		return acc.To(),nil
	}
}
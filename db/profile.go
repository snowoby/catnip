package db

import (
	"catnip/structs"
	"errors"
	"github.com/gin-gonic/gin"
)

func NewProfile(ctx *gin.Context, profile *structs.Profile,accountID int) (int,error) {
	p,err:=client.Profile.
		Create().
		SetName(profile.Name).
		SetFront(profile.Front).
		SetBehind(profile.Behind).
		SetOwnerID(accountID).
		SetCallable(profile.Callable).
		SetCallSign(profile.CallSign).
		Save(ctx)
	if err!=nil {
		return 0, err
	}else if p==nil {
		return 0,errors.New("result is nil")
	}else if p.ID==0 {
		return 0,errors.New("id is 0")
	}else {
		return p.ID, nil
	}
}
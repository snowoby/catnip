package main

import (
	"catnip/web"
	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
)

func init()  {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)

}

func main() {
	r:=gin.Default()
	web.Route(r)
	r.Run("127.0.0.1:80")
}
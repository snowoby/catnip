package web

import (
	"catnip/ent"
	"catnip/functions"
	"catnip/utils"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
)

const CookieDomain = "localhost"

func register (ctx *gin.Context) {
	var account ent.Account
	err:=ctx.ShouldBindJSON(&account)
	if err!=nil {
		log.Debug("form error: ", err)
		utils.QE(ctx,40000)
		return
	}
	if !utils.EmailValidator(account.Email) {
		log.Debug("email error: ", account.Email)
		utils.QE(ctx,40003)
		return
	}

	log.Debug("username is ", account.Username)
	if account.Username=="" {
		log.Debug("username is blank")
		utils.QE(ctx,40001)
		return
	}
	if !utils.PasswordValidator(account.Password) {
		utils.QE(ctx,40002)
		return
	}
	id,err:=functions.Register(ctx,&account)
	if err!=nil {
		log.Debug("register error, reason: ",err)
		utils.QE(ctx,40004)
		return
	}
	log.Debug("successfully registered, id: ",id)
	ctx.JSON(http.StatusOK,gin.H{ "id" : id })
	return
}

func login (ctx *gin.Context) {
	var account ent.Account
	err:=ctx.ShouldBindJSON(&account)
	if err!=nil {
		log.Debug("form error: ", err)
		utils.QE(ctx,40005)
		return
	}
	if account.Username=="" {
		log.Debug("username is blank")
		utils.QE(ctx,40006)
		return
	}

	if account.Password=="" {
		log.Debug("password is blank")
		utils.QE(ctx,40007)
		return
	}
	cookie,err:=functions.Login(ctx,&account)
	if err!=nil {
		log.Debug("login other error: ",err)
		utils.QE(ctx,40008)
		return
	}
	ctx.SetCookie("account",cookie.ToString(),int(cookie.Expire.Unix()),"/",CookieDomain,false,true)
	ctx.JSON(http.StatusOK,gin.H{
		"cookie":cookie.ToString(),
	})
	return
}

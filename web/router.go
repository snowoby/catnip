package web

import "github.com/gin-gonic/gin"

func Route(r *gin.Engine) {
	r.Use(CookieAuth())
	account:=r.Group("/account")
	account.POST("/register",register)
	account.POST("/login",login)

	r.GET("/ping",ping)
}
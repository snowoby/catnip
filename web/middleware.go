package web

import (
	"catnip/ent"
	"catnip/functions"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func CookieAuth() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		cookie, err := ctx.Cookie("account")
		log.Debug("cookie is ",cookie)
		if err!=nil {
			log.Debug(err)
		}else {
			account,err:=functions.VerifyCookie(ctx,cookie)
			if err!=nil {
				log.Debug(err)
			}else {
				log.Debug("login, ",account)
				ctx.Set("account",account)
			}
		}
		// before request
		ctx.Next()
		// after request

	}
}

func ping(ctx *gin.Context)  {
	account,ok:=ctx.Get("account")
	if !ok {
		log.Debug("not login")
		ctx.JSON(http.StatusUnauthorized,gin.H{})
	}else {
		a:=account.(*ent.Account)
		log.Debug("user: ", account)
		ctx.JSON(http.StatusOK,gin.H{"pong":a.Username})
	}
}